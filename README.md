# OpenML dataset: EgyptianSkulls

https://www.openml.org/d/1099

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Datasets of Data And Story Library, project illustrating use of basic statistic methods, converted to arff format by Hakan Kjellerstrand.
Source: TunedIT: http://tunedit.org/repo/DASL

DASL file http://lib.stat.cmu.edu/DASL/Datafiles/EgyptianSkulls.html

Egyptian Skull Development

Reference:   Thomson, A. and Randall-Maciver, R. (1905) Ancient Races of the Thebaid, Oxford:  Oxford University Press.
Also found in:  Hand, D.J., et al. (1994) A Handbook of Small Data Sets, New York:  Chapman & Hall, pp. 299-301.
Manly, B.F.J. (1986) Multivariate Statistical Methods, New York:  Chapman & Hall.
Authorization:   Contact Authors
Description:   Four measurements of male Egyptian skulls from 5 different time periods.  Thirty skulls are measured from each time period.


Number of cases:   150
Variable Names:

MB:   Maximal Breadth of Skull
BH:   Basibregmatic Height of Skull
BL:   Basialveolar Length of Skull
NH:   Nasal Height of Skull
Year:   Approximate Year of Skull Formation (negative = B.C., positive = A.D.)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1099) of an [OpenML dataset](https://www.openml.org/d/1099). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1099/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1099/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1099/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

